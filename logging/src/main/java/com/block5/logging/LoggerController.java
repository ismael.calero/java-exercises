package com.block5.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggerController {
    private static final Logger logger = LoggerFactory.getLogger(LoggerController.class);

    @RequestMapping("/")
    public String hello() {
        logger.info("This is a INFO ");
        logger.warn("This is a WARN ");
        logger.error("This is a ERROR");
        logger.debug("This is a DEBUG");
        logger.trace("This is a TRACE");

        return "Hola";
    }
}
