package com.block6.pathvariableheaders.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;


@Setter
@Getter
    public class AllDataResponse {
        private String body;
        private Map<String, String> requestParams;
        private List<String> headers;
}


