package com.block6.pathvariableheaders.infraestructure;

import com.block6.pathvariableheaders.domain.AllDataResponse;
import com.block6.pathvariableheaders.domain.Greeting;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/greeting")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @PostMapping("/addGreeting")
    public Greeting addGreeting(@RequestBody Greeting greeting) {
        return greeting;
    }

    @GetMapping("/user/{id}")
    public Greeting greeting(@PathVariable Long id) {
        return new Greeting(id, String.format(template, "World"));
    }

    @PutMapping("/post")
    public ResponseEntity<Map<String, Object>> updateGreeting(@RequestParam Map<String, String> params) {
        Map<String, Object> response = new HashMap<>(params);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/header")
    public ResponseEntity<String> getHeader(@RequestHeader HttpHeaders headers) {
        List<String> h1Values = headers.get("h1");
        List<String> h2Values = headers.get("h2");

        String response = "Headers:\n";
        if (h1Values != null) {
            response += "h1: " + String.join(", ", h1Values) + "\n";
        }
        if (h2Values != null) {
            response += "h2: " + String.join(", ", h2Values) + "\n";
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/all")
    public ResponseEntity<AllDataResponse> getAllData(@RequestBody(required = false) String body,
                                                      @RequestParam Map<String, String> params,
                                                      @RequestHeader HttpHeaders headers) {
        AllDataResponse response = new AllDataResponse();
        response.setBody(body);
        response.setRequestParams(params);
        response.setHeaders(extractHeaders(headers));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private List<String> extractHeaders(HttpHeaders headers) {
        return headers.entrySet().stream()
                .map(entry -> entry.getKey() + ": " + String.join(", ", entry.getValue()))
                .collect(Collectors.toList());
    }

}