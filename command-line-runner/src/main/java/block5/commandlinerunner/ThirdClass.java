package block5.commandlinerunner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ThirdClass implements CommandLineRunner {

    @Value("#{'${listOfValues}'}")
    private List<String> arguments;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Soy la tercera clase!");

        for (String arg : arguments) {
            System.out.println("Valor pasado como parámetro: " + arg);
        }
    }
}