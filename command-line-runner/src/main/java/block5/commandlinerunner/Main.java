package block5.commandlinerunner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;


@SpringBootApplication
public class Main {
    @Bean
    public FirstClass firstClass() {
        return new FirstClass();
    }

    @Bean
    @Order(1)
    public CommandLineRunner secondClass() {
        return new SecondClass();
    }

    @Bean
    @Order(2)
    public CommandLineRunner thirdClass() {
        return new ThirdClass();
    }


    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}

// El orden de los mensajes por consola son Hola desde clase inicial, Hola desde clase secundaria, Soy la tercera clase!

/*El primer mensaje que se muestra es “Hola desde clase inicial” ya que se utiliza @Postconstruct que se ejecutará
después de que el bean haya sido completamente construido y sus dependencias hayan sido inyectadas pero antes de que
la aplicación esté completamente inicializada.

Spring Boot justo después de arrancar la aplicación ejecuta los métodos run() de las clases que implementan
CommandLineRunner por lo que aparece "Soy la tercera clase" y "Hola desde clase secundaria". Esto ocurre porque @Bean
no asegura el orden de ejecución. Si queremos que la segunda clase se ejecute antes que la tercera podemos usar la
anotación @Order(1).*/
