package block5.commandlinerunner;

import jakarta.annotation.PostConstruct;

public class FirstClass {

    @PostConstruct
    public void primeraFuncion() {
        System.out.println("Hola desde clase inicial");
    }
}