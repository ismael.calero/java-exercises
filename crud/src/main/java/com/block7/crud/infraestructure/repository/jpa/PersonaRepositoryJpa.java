package com.block7.crud.infraestructure.repository.jpa;

import com.block7.crud.infraestructure.repository.jpa.entity.PersonaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonaRepositoryJpa extends JpaRepository<PersonaJpa, Integer> {

    List<PersonaJpa> findByNombre(String nombre);

}
