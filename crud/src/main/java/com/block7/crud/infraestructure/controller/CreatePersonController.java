package com.block7.crud.infraestructure.controller;

import com.block7.crud.application.CreatePersonaService;
import com.block7.crud.application.impl.CreatePersonaServiceImpl;
import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;
import com.block7.crud.infraestructure.controller.dto.output.PersonaOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class CreatePersonController {

    private final CreatePersonaService personaService;
    private final PersonaMapper personaMapper;

    @PostMapping
    public ResponseEntity<PersonaOutputDto> createPersona(@RequestBody PersonaInputDto persona) {

        return new ResponseEntity<>(personaMapper.toEntity(personaService.createPersona(persona)), HttpStatus.OK);
    }
}
