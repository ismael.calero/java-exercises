package com.block7.crud.infraestructure.controller;

import com.block7.crud.application.UpdatePersonaService;
import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;
import com.block7.crud.infraestructure.controller.dto.output.PersonaOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class UpdatePersonController {

    private final UpdatePersonaService personaService;
    private final PersonaMapper personaMapper;

    @PutMapping("/{id}")
    public ResponseEntity<PersonaOutputDto> updatePersona(@PathVariable Integer id, @RequestBody PersonaInputDto persona) {
        try {
            return new ResponseEntity<>(personaMapper.toEntity(personaService.updatePersona(id, persona)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
