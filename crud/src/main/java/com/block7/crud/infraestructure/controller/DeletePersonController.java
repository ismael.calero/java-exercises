package com.block7.crud.infraestructure.controller;

import com.block7.crud.application.DeletePersonaService;
import com.block7.crud.application.impl.DeletePersonaServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class DeletePersonController {

    private final DeletePersonaService personaService;

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePersonaById(@PathVariable int id)
    {
        try {
            personaService.deletePersonaById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
