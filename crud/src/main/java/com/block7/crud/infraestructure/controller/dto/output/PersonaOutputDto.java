package com.block7.crud.infraestructure.controller.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonaOutputDto {
    private Integer id;
    private String nombre;
    private String edad;
    private String poblacion;
}
