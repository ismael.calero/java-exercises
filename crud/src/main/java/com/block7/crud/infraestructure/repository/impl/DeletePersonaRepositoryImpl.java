package com.block7.crud.infraestructure.repository.impl;

import com.block7.crud.domain.repository.DeletePersonaRepository;
import com.block7.crud.infraestructure.repository.jpa.PersonaRepositoryJpa;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeletePersonaRepositoryImpl implements DeletePersonaRepository {

    private final PersonaRepositoryJpa personaRepositoryJpa;

    @Override
    public void deletePersonaById(Integer id) {
        personaRepositoryJpa.deleteById(id);
    }
}
