package com.block7.crud.infraestructure.repository.impl;

import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.CreatePersonaRepository;
import com.block7.crud.infraestructure.repository.jpa.PersonaRepositoryJpa;
import com.block7.crud.infraestructure.repository.jpa.entity.PersonaJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CreatePersonaRepositoryImpl implements CreatePersonaRepository {

    private final PersonaRepositoryJpa personaRepositoryJpa;
    private final PersonaMapper personaMapper;

    @Override
    public Persona createPersona(Persona persona) {

        PersonaJpa personaJpa = personaMapper.toEntityJpa(persona);

        return personaMapper.toEntity(personaRepositoryJpa.save(personaJpa));
    }
}
