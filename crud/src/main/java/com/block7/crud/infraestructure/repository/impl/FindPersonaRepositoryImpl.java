package com.block7.crud.infraestructure.repository.impl;

import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.FindPersonaRepository;
import com.block7.crud.infraestructure.repository.jpa.PersonaRepositoryJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class FindPersonaRepositoryImpl implements FindPersonaRepository {

    private final PersonaRepositoryJpa personaRepositoryJpa;
    private final PersonaMapper personaMapper;


    @Override
    public Persona getPersonaById(Integer id) throws Exception {

        return personaMapper.toEntity(personaRepositoryJpa.findById(id).orElseThrow(() ->
                new Exception("Persona not found")));
    }

    @Override
    public List<Persona> getPersonaByName(String nombre) {

        return personaMapper.toEntity(personaRepositoryJpa.findByNombre(nombre));
    }

    @Override
    public Page<Persona> getAllPersonas(Integer pageNumber, Integer pageSize) {

        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);

        return personaRepositoryJpa.findAll(pageable)
                .map(personaMapper::toEntity);
    }
}
