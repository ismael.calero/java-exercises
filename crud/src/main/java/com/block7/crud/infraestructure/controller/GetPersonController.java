package com.block7.crud.infraestructure.controller;

import com.block7.crud.application.FindPersonaService;
import com.block7.crud.application.impl.FindPersonaServiceImpl;
import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.infraestructure.controller.dto.output.PersonaOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class GetPersonController {

    private final FindPersonaService personaService;
    private final PersonaMapper personaMapper;

    @GetMapping("/{id}")
    public ResponseEntity<PersonaOutputDto> getPersonaById(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(personaMapper.toEntity(personaService.getPersonaById(id)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/nombre/{nombre}")
    public ResponseEntity<List<PersonaOutputDto>> getPersonaByName(@PathVariable String nombre) {
        try {
            return new ResponseEntity<>(personaMapper.toOutput(personaService.getPersonaByName(nombre)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public ResponseEntity<Page<PersonaOutputDto>> getAllPersonas(@RequestParam(defaultValue = "0", required = false)
                                                     Integer pageNumber,
                                                                @RequestParam(defaultValue = "5", required = false)
                                                     Integer pageSize) {
        return new ResponseEntity<>(personaService.getAllPersonas(pageNumber, pageSize).map(personaMapper::toEntity), HttpStatus.OK);
    }
}
