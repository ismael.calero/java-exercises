package com.block7.crud.application.impl;

import com.block7.crud.application.CreatePersonaService;
import com.block7.crud.application.mapper.PersonaMapper;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.CreatePersonaRepository;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreatePersonaServiceImpl implements CreatePersonaService {

    private final CreatePersonaRepository createPersonaRepository;
    private final PersonaMapper personaMapper;

    @Override
    public Persona createPersona(PersonaInputDto personaInput) {

        Persona persona = personaMapper.toEntity(personaInput);

        return createPersonaRepository.createPersona(persona);
    }
}
