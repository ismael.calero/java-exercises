package com.block7.crud.application.impl;

import com.block7.crud.application.FindPersonaService;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.FindPersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindPersonaServiceImpl implements FindPersonaService {

    private final FindPersonaRepository personaRepository;

    @Override
    public Persona getPersonaById(Integer id) throws Exception {

        return personaRepository.getPersonaById(id);
    }

    @Override
    public List<Persona> getPersonaByName(String nombre) {
        return personaRepository.getPersonaByName(nombre);

    }

    @Override
    public Page<Persona> getAllPersonas(Integer pageNumber, Integer pageSize) {

        return personaRepository.getAllPersonas(pageNumber, pageSize);
    }
}
