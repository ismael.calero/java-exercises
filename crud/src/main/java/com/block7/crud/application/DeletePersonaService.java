package com.block7.crud.application;

import com.block7.crud.domain.entity.Persona;

public interface DeletePersonaService {
    void deletePersonaById(Integer id);
}
