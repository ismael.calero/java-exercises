package com.block7.crud.application.impl;

import com.block7.crud.application.DeletePersonaService;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.DeletePersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeletePersonaServiceImpl implements DeletePersonaService {

    private final DeletePersonaRepository personaRepository;

    @Override
    public void deletePersonaById(Integer id) {
        personaRepository.deletePersonaById(id);
    }
}

