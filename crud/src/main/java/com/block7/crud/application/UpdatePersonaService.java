package com.block7.crud.application;

import com.block7.crud.domain.entity.Persona;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;

public interface UpdatePersonaService {
    Persona updatePersona(Integer id, PersonaInputDto persona) throws Exception;
}
