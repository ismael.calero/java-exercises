package com.block7.crud.application.impl;

import com.block7.crud.application.FindPersonaService;
import com.block7.crud.application.UpdatePersonaService;
import com.block7.crud.domain.entity.Persona;
import com.block7.crud.domain.repository.CreatePersonaRepository;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UpdatePersonaServiceImpl implements UpdatePersonaService {

    private final CreatePersonaRepository createPersonaRepository;
    private final FindPersonaService personaService;

    @Override
    public Persona updatePersona(Integer id, PersonaInputDto personaInputDto) throws Exception {

        Persona persona = personaService.getPersonaById(id);

        if (personaInputDto.getNombre() != null) {
            persona.setNombre(personaInputDto.getNombre());
        }
        if (personaInputDto.getEdad() != null) {
            persona.setEdad(personaInputDto.getEdad());
        }
        if (personaInputDto.getPoblacion() != null) {
            persona.setPoblacion(personaInputDto.getPoblacion());
        }

        return createPersonaRepository.createPersona(persona);
    }
}

