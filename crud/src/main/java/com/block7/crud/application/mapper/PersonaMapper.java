package com.block7.crud.application.mapper;

import com.block7.crud.domain.entity.Persona;
import com.block7.crud.infraestructure.controller.dto.input.PersonaInputDto;
import com.block7.crud.infraestructure.controller.dto.output.PersonaOutputDto;
import com.block7.crud.infraestructure.repository.jpa.entity.PersonaJpa;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class PersonaMapper {


    public abstract Persona toEntity(PersonaInputDto persona);
    public abstract PersonaOutputDto toEntity(Persona persona);
    public abstract List<PersonaOutputDto> toOutput(List<Persona> persona);

    public abstract PersonaJpa toEntityJpa(Persona persona);
    public abstract Persona toEntity(PersonaJpa persona);
    public abstract List<Persona> toEntity(List<PersonaJpa> persona);
}

