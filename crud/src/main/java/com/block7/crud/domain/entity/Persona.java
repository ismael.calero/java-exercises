package com.block7.crud.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Persona {

    private Integer id;

    private String nombre;

    private String edad;

    private String poblacion;

}
