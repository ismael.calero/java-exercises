package com.block7.crud.domain.repository;

import com.block7.crud.domain.entity.Persona;
import org.springframework.data.domain.Page;

import java.util.List;

public interface FindPersonaRepository {
    Persona getPersonaById(Integer id) throws Exception;
    List<Persona> getPersonaByName(String nombre);
    Page<Persona> getAllPersonas(Integer pageNumber, Integer pageSize);
}
