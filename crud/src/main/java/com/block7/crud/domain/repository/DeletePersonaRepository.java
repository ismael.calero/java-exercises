package com.block7.crud.domain.repository;

import com.block7.crud.domain.entity.Persona;

public interface DeletePersonaRepository {
    void deletePersonaById(Integer id);
}
