package com.block8.uploaddownloadfiles.service;

import com.block8.uploaddownloadfiles.model.File;
import com.block8.uploaddownloadfiles.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class FileService {

    private final FileRepository fileRepository;

    public void uploadFile(String fileName, String category) {
        File file = new File();
        file.setFileName(fileName);
        file.setCategory(category);
        file.setUploadDate(LocalDateTime.now());
        fileRepository.save(file);
    }

    public File downloadFileById(Long id) {
        return fileRepository.findById(id).orElse(null);
    }

    public File downloadFileByName(String fileName) {
        return fileRepository.findByFileName(fileName);
    }
}
