package com.block8.uploaddownloadfiles.repository;

import com.block8.uploaddownloadfiles.model.File;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FileRepository extends JpaRepository<File, Long> {
    File findByFileName(String fileName);
}
