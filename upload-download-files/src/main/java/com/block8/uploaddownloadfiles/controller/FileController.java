package com.block8.uploaddownloadfiles.controller;

import com.block8.uploaddownloadfiles.model.File;
import com.block8.uploaddownloadfiles.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@RestController
@RequiredArgsConstructor
@RequestMapping("/files")
public class FileController {

    private final FileService fileService;

    @PostMapping("/upload/{tipo}")
    public ResponseEntity<?> uploadFile( @PathVariable String tipo,
                                         @RequestParam("file") MultipartFile file,
                                         @RequestParam("category") String category) {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body("No se ha seleccionado ningún archivo.");
        }
        String fileName = file.getOriginalFilename();
        assert fileName != null;
        if (!fileName.endsWith(tipo)) {
            return ResponseEntity.badRequest().body("El archivo debe tener la extensión " + tipo);
        }

        try {
            // Guardar el archivo en el directorio configurado
            Path path = Paths.get("c:/tmp", fileName);
            Files.copy(file.getInputStream(), path);

            // Guardar en la base de datos y responder
            fileService.uploadFile(fileName, category);
            return ResponseEntity.ok("Archivo subido exitosamente en: " + path.toString());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al subir el archivo.");
        }
    }

    @GetMapping("/setpath")
    public ResponseEntity<String> setPath(@RequestParam("path") String path) {
        if (path == null) {
            return ResponseEntity.badRequest().body("El directorio especificado no existe.");
        }

        return ResponseEntity.ok("Directorio de guardado actualizado a: " + path);
    }

    @GetMapping("/download/{id}")
    public ResponseEntity<Resource> downloadFileById(@PathVariable Long id) {
        File file = fileService.downloadFileById(id);
        if (file == null) {
            return ResponseEntity.notFound().build();
        }
        try {
            String filePath = "c:/tmp/" + file.getFileName();

            Resource resource = new FileSystemResource(filePath);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getFileName());

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadFileByName(@RequestParam String fileName) {
        File file = fileService.downloadFileByName(fileName);
        if (file == null) {
            return ResponseEntity.notFound().build();
        }
        try {
            String filePath = "c:/tmp/" + file.getFileName();
            // Lee el archivo en un InputStream
            InputStreamResource resource = new InputStreamResource(new FileInputStream(filePath));

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getFileName());

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
