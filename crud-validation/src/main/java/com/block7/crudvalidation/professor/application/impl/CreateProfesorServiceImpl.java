package com.block7.crudvalidation.professor.application.impl;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.application.service.CreateProfesorService;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.repository.CreateProfesorRepository;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class CreateProfesorServiceImpl implements CreateProfesorService {

    private final CreateProfesorRepository createProfesorRepository;
    private final ProfesorMapper profesorMapper;

    @Override
    public Profesor createProfesor(ProfesorInputDto profesorInput) throws ResponseStatusException {

        validateProfesorInput(profesorInput);

        Profesor profesor = profesorMapper.toEntity(profesorInput);

        return createProfesorRepository.createProfesor(profesor);
    }

    public void validateProfesorInput(ProfesorInputDto profesorInput) throws ResponseStatusException {
        if (profesorInput.getBranch() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'branch' no puede ser nulo");
        }
    }

}
