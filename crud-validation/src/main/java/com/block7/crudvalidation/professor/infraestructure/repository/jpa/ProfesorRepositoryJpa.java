package com.block7.crudvalidation.professor.infraestructure.repository.jpa;

import com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity.ProfesorJpa;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProfesorRepositoryJpa extends JpaRepository<ProfesorJpa, Integer> {
}
