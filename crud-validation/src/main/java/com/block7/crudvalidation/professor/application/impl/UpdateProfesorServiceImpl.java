package com.block7.crudvalidation.professor.application.impl;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.application.service.CreateProfesorService;
import com.block7.crudvalidation.professor.application.service.GetProfesorService;
import com.block7.crudvalidation.professor.application.service.UpdateProfesorService;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.repository.CreateProfesorRepository;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
@AllArgsConstructor
public class UpdateProfesorServiceImpl implements UpdateProfesorService {

    private final CreateProfesorService createProfesorService;
    private final CreateProfesorRepository profesorRepository;
    private final GetProfesorService profesorService;
    private final ProfesorMapper profesorMapper;

    @Override
    public Profesor updateProfesor(Integer id, ProfesorInputDto profesorInputDto) throws ResponseStatusException {

        Profesor profesor = profesorService.getProfesorById(id);

        createProfesorService.validateProfesorInput(profesorInputDto);

        profesorMapper.updateToEntity(profesorInputDto, profesor);

        return profesorRepository.createProfesor(profesor);
    }
}

