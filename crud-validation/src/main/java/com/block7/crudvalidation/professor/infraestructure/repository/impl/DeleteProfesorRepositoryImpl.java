package com.block7.crudvalidation.professor.infraestructure.repository.impl;

import com.block7.crudvalidation.professor.domain.repository.DeleteProfesorRepository;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeleteProfesorRepositoryImpl implements DeleteProfesorRepository {

    private final ProfesorRepositoryJpa profesorRepositoryJpa;

    @Override
    public void deleteProfesorById(Integer id) {
        profesorRepositoryJpa.deleteById(id);
    }
}
