package com.block7.crudvalidation.professor.infraestructure.controller.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProfesorOutputDto {

    Integer idProfesor;

    Integer idPersona;

    String comments;

    String branch;
}
