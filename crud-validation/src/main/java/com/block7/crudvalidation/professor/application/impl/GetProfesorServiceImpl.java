package com.block7.crudvalidation.professor.application.impl;

import com.block7.crudvalidation.person.application.service.feign.FeignProfesorClient;
import com.block7.crudvalidation.professor.application.service.GetProfesorService;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.repository.GetProfesorRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class GetProfesorServiceImpl implements GetProfesorService {

    private final GetProfesorRepository profesorRepository;
    private final FeignProfesorClient feignInterface;

    public Profesor getProfesorById(Integer id) {

        return profesorRepository.getProfesorById(id);
    }

    @Override
    public Page<Profesor> getAllProfesor(Integer pageNumber, Integer pageSize) {
        return profesorRepository.getAllProfesor(pageNumber, pageSize);
    }
}
