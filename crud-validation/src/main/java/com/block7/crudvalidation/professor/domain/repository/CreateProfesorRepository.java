package com.block7.crudvalidation.professor.domain.repository;

import com.block7.crudvalidation.professor.domain.entity.Profesor;

public interface CreateProfesorRepository {
    Profesor createProfesor(Profesor profesor);
}
