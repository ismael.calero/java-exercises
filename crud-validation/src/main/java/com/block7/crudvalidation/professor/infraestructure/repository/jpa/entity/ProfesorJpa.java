package com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity;

import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "profesor")
public class ProfesorJpa {
    @Id
    @Column(name = "id_profesor")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer idProfesor;

    @OneToOne
    @JoinColumn(name = "id_persona")
    private PersonJpa persona;

    private String comments;

    @Column(nullable = false)
    private String branch;

    @OneToMany(mappedBy = "profesor", fetch = FetchType.LAZY)
    private List<StudentJpa> estudiantes;

}
