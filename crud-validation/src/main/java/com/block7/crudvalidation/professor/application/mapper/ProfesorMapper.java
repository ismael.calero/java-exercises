package com.block7.crudvalidation.professor.application.mapper;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.exception.CustomError;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity.ProfesorJpa;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;


@Mapper(componentModel = "spring", uses = PersonMapper.class)
public abstract class ProfesorMapper {

    public abstract Profesor toEntity(ProfesorInputDto profesor);
    public abstract ProfesorOutputDto toOutput(Profesor profesor);
    public abstract List<ProfesorOutputDto> toListOutput(List<Profesor> profesor);

    public abstract ProfesorJpa toEntityJpa(Profesor profesor);
    @Mapping(target = "idPersona", source = "profesor.persona.idPersona")
    public abstract Profesor toEntityfromJpa(ProfesorJpa profesor);
    public abstract List<Profesor> toListEntity(List<ProfesorJpa> profesor);

    public abstract ProfesorOutputDto toOutputfromError(CustomError error);

    //@Mapping(target = "name", source = "firstName")
    //@Mapping(target = "id_persona", expression = "java( person.getSurname() != null ? person.setSurname(personInputDto.getSurname()))")
    //@AfterMapping
    public abstract void updateToEntity(ProfesorInputDto personInputDto, @MappingTarget Profesor profesor);
}
