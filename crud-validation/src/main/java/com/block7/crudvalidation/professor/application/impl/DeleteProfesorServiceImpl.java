package com.block7.crudvalidation.professor.application.impl;

import com.block7.crudvalidation.professor.application.service.DeleteProfesorService;
import com.block7.crudvalidation.professor.application.service.GetProfesorService;
import com.block7.crudvalidation.professor.domain.repository.DeleteProfesorRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteProfesorServiceImpl implements DeleteProfesorService {

    private final DeleteProfesorRepository profesorRepository;
    private final GetProfesorService profesorService;

    @Override
    public void deleteProfesorById(Integer id) throws EntityNotFoundException {

        profesorService.getProfesorById(id);

        profesorRepository.deleteProfesorById(id);
    }
}
