package com.block7.crudvalidation.professor.infraestructure.repository.impl;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.repository.GetProfesorRepository;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class GetProfesorRepositoryImpl implements GetProfesorRepository {

    private final ProfesorRepositoryJpa profesorRepositoryJpa;
    private final ProfesorMapper profesorMapper;


    @Override
    public Profesor getProfesorById(Integer id) throws EntityNotFoundException {

        return profesorMapper.toEntityfromJpa(profesorRepositoryJpa.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Profesor no encontrado")));
    }

    @Override
    public Page<Profesor> getAllProfesor(Integer pageNumber, Integer pageSize) {

        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);

        return profesorRepositoryJpa.findAll(pageable).map(profesorMapper::toEntityfromJpa);
    }
}
