package com.block7.crudvalidation.professor.infraestructure.repository.impl;

import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.PersonRepositoryJpa;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.domain.repository.CreateProfesorRepository;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity.ProfesorJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CreateProfesorRepositoryImpl implements CreateProfesorRepository {

    private final ProfesorRepositoryJpa profesorRepositoryJpa;
    private final PersonRepositoryJpa personRepositoryJpa;
    private final ProfesorMapper profesorMapper;

    @Override
    public Profesor createProfesor(Profesor profesor) {

        PersonJpa person = personRepositoryJpa.findById(profesor.getIdPersona()).orElse(null);
        ProfesorJpa profesorJpa = profesorMapper.toEntityJpa(profesor);

        profesorJpa.setPersona(person);

        return profesorMapper.toEntityfromJpa(profesorRepositoryJpa.save(profesorJpa));
    }
}
