package com.block7.crudvalidation.professor.infraestructure.controller;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.application.service.UpdateProfesorService;
import com.block7.crudvalidation.professor.domain.exception.CustomError;
import com.block7.crudvalidation.professor.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/profesor")
@RequiredArgsConstructor
public class UpdateProfesorController {

    private final UpdateProfesorService profesorService;
    private final ProfesorMapper profesorMapper;

    @PutMapping("/{id}")
    public ResponseEntity<ProfesorOutputDto> updateProfesor(@PathVariable Integer id, @RequestBody ProfesorInputDto profesor) throws Exception {
        try {
            return new ResponseEntity<>(profesorMapper.toOutput(profesorService.updateProfesor(id, profesor)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(profesorMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(profesorMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
