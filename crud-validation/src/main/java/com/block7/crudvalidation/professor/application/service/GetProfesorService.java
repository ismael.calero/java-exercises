package com.block7.crudvalidation.professor.application.service;


import com.block7.crudvalidation.professor.domain.entity.Profesor;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;


public interface GetProfesorService {
    Profesor getProfesorById(Integer id) throws EntityNotFoundException;
    Page<Profesor> getAllProfesor(Integer pageNumber, Integer pageSize);
}
