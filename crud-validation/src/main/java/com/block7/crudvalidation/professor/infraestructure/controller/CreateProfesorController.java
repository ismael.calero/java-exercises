package com.block7.crudvalidation.professor.infraestructure.controller;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.application.service.CreateProfesorService;
import com.block7.crudvalidation.professor.domain.exception.CustomError;
import com.block7.crudvalidation.professor.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/profesor")
@RequiredArgsConstructor
public class CreateProfesorController {

    private final CreateProfesorService personaService;
    private final ProfesorMapper personaMapper;

    @PostMapping
    public ResponseEntity<ProfesorOutputDto> createPerson(@RequestBody ProfesorInputDto profesor) throws Exception {
        try {
            ProfesorOutputDto outputDto = personaMapper.toOutput(personaService.createProfesor(profesor));
            return new ResponseEntity<>(outputDto, HttpStatus.OK);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(personaMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}