package com.block7.crudvalidation.professor.domain.repository;

import com.block7.crudvalidation.professor.domain.entity.Profesor;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;


public interface GetProfesorRepository {
    Profesor getProfesorById(Integer id) throws EntityNotFoundException;
    Page<Profesor> getAllProfesor(Integer pageNumber, Integer pageSize);
}
