package com.block7.crudvalidation.professor.infraestructure.controller;

import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.application.service.GetProfesorService;
import com.block7.crudvalidation.professor.domain.exception.CustomError;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/profesor")
@RequiredArgsConstructor
public class GetProfesorController {

    private final GetProfesorService profesorService;
    private final ProfesorMapper profesorMapper;

    @GetMapping("/{id}")
    public ResponseEntity<ProfesorOutputDto> getProfesorById(@PathVariable Integer id) throws Exception {
        try {
            return new ResponseEntity<>(profesorMapper.toOutput(profesorService.getProfesorById(id)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
        CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
        return new ResponseEntity<>(profesorMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
    }
    }

    @GetMapping
    public ResponseEntity<Page<ProfesorOutputDto>> getAllProfesor(@RequestParam(defaultValue = "0", required = false)
                                                                 Integer pageNumber,
                                                              @RequestParam(defaultValue = "5", required = false)
                                                                 Integer pageSize) {
        return new ResponseEntity<>(profesorService.getAllProfesor(pageNumber, pageSize).map(profesorMapper::toOutput), HttpStatus.OK);
    }
}
