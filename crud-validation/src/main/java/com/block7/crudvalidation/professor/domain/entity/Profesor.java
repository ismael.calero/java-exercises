package com.block7.crudvalidation.professor.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Profesor {

    Integer idProfesor;

    Integer idPersona;

    String comments;

    String branch;
}
