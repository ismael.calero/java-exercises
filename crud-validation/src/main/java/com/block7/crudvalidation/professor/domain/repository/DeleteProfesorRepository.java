package com.block7.crudvalidation.professor.domain.repository;

public interface DeleteProfesorRepository {
    void deleteProfesorById(Integer id);
}
