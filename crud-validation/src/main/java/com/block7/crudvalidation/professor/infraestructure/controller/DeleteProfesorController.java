package com.block7.crudvalidation.professor.infraestructure.controller;

import com.block7.crudvalidation.professor.application.service.DeleteProfesorService;
import com.block7.crudvalidation.professor.domain.exception.CustomError;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/profesor")
@RequiredArgsConstructor
public class DeleteProfesorController {

    private final DeleteProfesorService profesorService;

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProfesorById(@PathVariable int id) throws Exception {
        try {
            profesorService.deleteProfesorById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(error.toString(), HttpStatus.NOT_FOUND);
        }
    }
}
