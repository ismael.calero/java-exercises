package com.block7.crudvalidation.professor.application.service;


import com.block7.crudvalidation.professor.domain.entity.Profesor;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.input.ProfesorInputDto;
import org.springframework.web.server.ResponseStatusException;

public interface UpdateProfesorService {
    Profesor updateProfesor(Integer id, ProfesorInputDto profesor) throws ResponseStatusException;
}
