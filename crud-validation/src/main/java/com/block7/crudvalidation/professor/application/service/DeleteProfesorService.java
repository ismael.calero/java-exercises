package com.block7.crudvalidation.professor.application.service;

import jakarta.persistence.EntityNotFoundException;

public interface DeleteProfesorService {
    void deleteProfesorById(Integer id) throws EntityNotFoundException;
}
