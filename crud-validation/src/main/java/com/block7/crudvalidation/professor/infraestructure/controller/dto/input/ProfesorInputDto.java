package com.block7.crudvalidation.professor.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProfesorInputDto {

    Integer idProfesor;

    Integer idPersona;

    String comments;

    String branch;
}

