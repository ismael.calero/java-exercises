package com.block7.crudvalidation.person.infraestructure.controller;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.CreatePersonService;
import com.block7.crudvalidation.person.domain.exception.CustomError;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import com.block7.crudvalidation.person.infraestructure.controller.dto.output.PersonOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.block7.crudvalidation.person.domain.exception.UnprocessableEntityException;

import java.util.Date;


@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class CreatePersonController {

    private final CreatePersonService personaService;
    private final PersonMapper personaMapper;


    @CrossOrigin
    @PostMapping("/addperson")
    public ResponseEntity<PersonOutputDto> createPerson(@RequestBody PersonInputDto person) throws Exception {
        try {
            PersonOutputDto outputDto = personaMapper.toOutput(personaService.createPerson(person));
            return new ResponseEntity<>(outputDto, HttpStatus.OK);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(personaMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}