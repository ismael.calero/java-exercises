package com.block7.crudvalidation.person.infraestructure.repository.impl;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.domain.repository.GetPersonRepository;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.PersonRepositoryJpa;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class GetPersonRepositoryImpl implements GetPersonRepository {

    private final PersonRepositoryJpa personaRepositoryJpa;
    private final PersonMapper personaMapper;


    @Override
    public Person getPersonById(Integer id) throws EntityNotFoundException {

        return personaMapper.toEntityfromJpa(personaRepositoryJpa.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Persona no encontrada")));
    }

    @Override
    public List<Person> getPersonByUsuario(String usuario) {

        return personaMapper.toListEntity(personaRepositoryJpa.findByUsuario(usuario));
    }

    @Override
    public Page<Person> getAllPeople(Integer pageNumber, Integer pageSize) {

        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);

        return personaRepositoryJpa.findAll(pageable).map(personaMapper::toEntityfromJpa);
    }
}
