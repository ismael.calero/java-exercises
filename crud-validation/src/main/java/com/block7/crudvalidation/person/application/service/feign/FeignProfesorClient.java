package com.block7.crudvalidation.person.application.service.feign;

import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(url = "http://localhost:8081", name = "MyFeign")
public interface FeignProfesorClient {

    @GetMapping("/profesor/{id}")
    ProfesorOutputDto getProfesorById(@PathVariable("id") Integer id);
}
