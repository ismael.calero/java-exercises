package com.block7.crudvalidation.person.application.impl;

import com.block7.crudvalidation.person.application.service.GetPersonService;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.domain.repository.GetPersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class GetPersonServiceImpl implements GetPersonService {

    private final GetPersonRepository personRepository;

    @Override
    public Person getPersonById(Integer id) {
        return personRepository.getPersonById(id);
    }

    @Override
    public List<Person> getPersonByUsuario(String usuario) {
        return personRepository.getPersonByUsuario(usuario);

    }

    @Override
    public Page<Person> getAllPeople(Integer pageNumber, Integer pageSize) {
        return personRepository.getAllPeople(pageNumber, pageSize);
    }
}