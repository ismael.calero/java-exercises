package com.block7.crudvalidation.person.infraestructure.controller;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.UpdatePersonService;
import com.block7.crudvalidation.person.domain.exception.CustomError;
import com.block7.crudvalidation.person.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import com.block7.crudvalidation.person.infraestructure.controller.dto.output.PersonOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class UpdatePersonController {

    private final UpdatePersonService personaService;
    private final PersonMapper personaMapper;

    @PutMapping("/{id}")
    public ResponseEntity<PersonOutputDto> updatePerson(@PathVariable Integer id, @RequestBody PersonInputDto person) throws Exception {
        try {
            return new ResponseEntity<>(personaMapper.toOutput(personaService.updatePerson(id, person)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(personaMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(personaMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
