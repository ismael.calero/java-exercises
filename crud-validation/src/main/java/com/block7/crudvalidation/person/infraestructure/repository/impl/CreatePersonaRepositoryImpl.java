package com.block7.crudvalidation.person.infraestructure.repository.impl;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.domain.repository.CreatePersonRepository;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.PersonRepositoryJpa;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Repository
@RequiredArgsConstructor
public class CreatePersonaRepositoryImpl implements CreatePersonRepository {

    private final PersonRepositoryJpa personaRepositoryJpa;
    private final PersonMapper personaMapper;

    @Override
    public Person createPerson(Person person) {

        PersonJpa personJpa = personaMapper.toEntityJpa(person);

        return personaMapper.toEntityfromJpa(personaRepositoryJpa.save(personJpa));
    }
}
