package com.block7.crudvalidation.person.infraestructure.controller;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.GetPersonService;
import com.block7.crudvalidation.person.application.service.feign.FeignProfesorClient;
import com.block7.crudvalidation.person.domain.exception.CustomError;
import com.block7.crudvalidation.person.infraestructure.controller.dto.output.PersonOutputDto;
import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.professor.infraestructure.controller.dto.output.ProfesorOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class GetPersonController {

    private final GetPersonService personaService;
    private final PersonMapper personMapper;
    private final ProfesorMapper profesorMapper;
    private final FeignProfesorClient profesorClient;

    @GetMapping("/{id}")
    public ResponseEntity<PersonOutputDto> getPersonById(@PathVariable Integer id) throws Exception {
        try {
            return new ResponseEntity<>(personMapper.toOutput(personaService.getPersonById(id)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
        CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
        return new ResponseEntity<>(personMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
    }
    }

    @GetMapping("/nombre/{usuario}")
    public ResponseEntity<List<PersonOutputDto>> getPersonByUsuario(@PathVariable String usuario) {
        try {
            return new ResponseEntity<>(personMapper.toListOutput(personaService.getPersonByUsuario(usuario)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @GetMapping("/getall")
    public ResponseEntity<Page<PersonOutputDto>> getAllPeople(@RequestParam(defaultValue = "0", required = false)
                                                                 Integer pageNumber,
                                                                 @RequestParam(defaultValue = "5", required = false)
                                                                 Integer pageSize) {
        return new ResponseEntity<>(personaService.getAllPeople(pageNumber, pageSize).map(personMapper::toOutput), HttpStatus.OK);
    }

    @GetMapping("/profesor/{id}")
    public ResponseEntity<ProfesorOutputDto> getProfesorByID(@PathVariable Integer id) throws Exception {
        try {
            ProfesorOutputDto profesor = profesorClient.getProfesorById(id);
            return new ResponseEntity<>(profesor, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            com.block7.crudvalidation.professor.domain.exception.CustomError error =
                    new com.block7.crudvalidation.professor.domain.exception.CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(profesorMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        }
    }
}
