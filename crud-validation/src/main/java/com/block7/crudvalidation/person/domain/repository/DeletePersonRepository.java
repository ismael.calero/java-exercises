package com.block7.crudvalidation.person.domain.repository;

public interface DeletePersonRepository {
    void deletePersonById(Integer id);
}
