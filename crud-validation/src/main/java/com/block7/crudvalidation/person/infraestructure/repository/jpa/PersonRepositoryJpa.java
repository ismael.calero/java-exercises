package com.block7.crudvalidation.person.infraestructure.repository.jpa;

import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PersonRepositoryJpa extends JpaRepository<PersonJpa, Integer> {
    List<PersonJpa> findByUsuario(String usuario);
}
