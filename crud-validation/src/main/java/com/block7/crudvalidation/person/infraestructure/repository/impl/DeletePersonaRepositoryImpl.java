package com.block7.crudvalidation.person.infraestructure.repository.impl;

import com.block7.crudvalidation.person.domain.repository.DeletePersonRepository;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.PersonRepositoryJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeletePersonaRepositoryImpl implements DeletePersonRepository {

    private final PersonRepositoryJpa personaRepositoryJpa;

    @Override
    public void deletePersonById(Integer id) {
        personaRepositoryJpa.deleteById(id);
    }
}
