package com.block7.crudvalidation.person.infraestructure.repository.jpa.entity;

import com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity.ProfesorJpa;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "persona")
public class PersonJpa {
    @Id
    @GeneratedValue
    @Column(name = "id_persona", nullable = false)
    private Integer idPersona;

    private String usuario;

    private String password;

    private String name;

    private String surname;

    @Column(name = "company_email")
    private String companyEmail;

    @Column(name = "personal_email")
    private String personalEmail;

    private String city;

    private Boolean active;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "imagen_url")
    private String imagenUrl;

    @Column(name = "termination_date")
    private Date terminationDate;

    @OneToOne(mappedBy = "persona")
    private ProfesorJpa profesor;

    @OneToOne(mappedBy = "persona")
    private StudentJpa student;
}
