package com.block7.crudvalidation.person.application.service;

import jakarta.persistence.EntityNotFoundException;

public interface DeletePersonService {
    void deletePersonById(Integer id) throws EntityNotFoundException;
}
