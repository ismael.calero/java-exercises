package com.block7.crudvalidation.person.application.impl;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.CreatePersonService;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.domain.repository.CreatePersonRepository;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class CreatePersonServiceImpl implements CreatePersonService {

    private final CreatePersonRepository createPersonRepository;
    private final PersonMapper personMapper;

    @Override
    public Person createPerson(PersonInputDto personInput) throws ResponseStatusException {

        validatePersonInput(personInput);

        Person person = personMapper.toEntity(personInput);

        return createPersonRepository.createPerson(person);
    }

    public void validatePersonInput(PersonInputDto personInput) throws ResponseStatusException {
//        Validacion de usuario
        if (personInput.getUsuario() == null || personInput.getUsuario().length() < 6 || personInput.getUsuario().length() > 10) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'usuario' debe tener entre 6 y 10 caracteres");
        }
//        Validación de Contraseña
        if (personInput.getPassword() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'password' no puede ser nulo");
        }
//        Validación de Nombre
        if (personInput.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'nombre' no puede ser nulo");
        }
//        Validación de Email de la compañía
        if (personInput.getCompanyEmail() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'correo electrónico de empresa' no puede ser nulo");
        }
//        Validación de Email personal
        if (personInput.getPersonalEmail() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'correo electrónico personal' no puede ser nulo");
        }
//        Validación de Ciudad
        if (personInput.getCity() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'ciudad' no puede ser nulo");
        }
//        Validación de Active
        if (personInput.getActive() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El estado activo no puede ser nulo");
        }
//        Validación de Date
        if (personInput.getCreatedDate() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"La fecha de creación no puede ser nula");
        }
    }

}
