package com.block7.crudvalidation.person.application.service;

import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import org.springframework.web.server.ResponseStatusException;


public interface UpdatePersonService {
    Person updatePerson(Integer id, PersonInputDto persona) throws ResponseStatusException;
}
