package com.block7.crudvalidation.person.application.mapper;

import com.block7.crudvalidation.person.domain.exception.CustomError;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import com.block7.crudvalidation.person.infraestructure.controller.dto.output.PersonOutputDto;
import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import com.block7.crudvalidation.person.domain.entity.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;


@Mapper(componentModel = "spring")
public abstract class PersonMapper {

    public abstract Person toEntity(PersonInputDto person);
    public abstract PersonOutputDto toOutput(Person persona);
    public abstract List<PersonOutputDto> toListOutput(List<Person> persona);

    public abstract PersonJpa toEntityJpa(Person persona);
    public abstract Person toEntityfromJpa(PersonJpa persona);
    public abstract List<Person> toListEntity(List<PersonJpa> persona);

    public abstract PersonOutputDto toOutputfromError(CustomError error);

    //@Mapping(target = "name", source = "firstName")
    //@Mapping(target = "id_persona", expression = "java( person.getSurname() != null ? person.setSurname(personInputDto.getSurname()))")
    //@AfterMapping
    @Mapping(target = "surname", ignore = true)
    @Mapping(target = "imagenUrl", ignore = true)
    @Mapping(target = "terminationDate", ignore = true)
    public abstract void updateToEntity(PersonInputDto personInputDto, @MappingTarget Person person);
}
