package com.block7.crudvalidation.person.application.service;

import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import org.springframework.web.server.ResponseStatusException;

public interface CreatePersonService {
    Person createPerson(PersonInputDto person) throws ResponseStatusException;
    void validatePersonInput(PersonInputDto personInput) throws ResponseStatusException;
}

