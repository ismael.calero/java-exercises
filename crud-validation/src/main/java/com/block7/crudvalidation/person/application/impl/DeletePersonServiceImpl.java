package com.block7.crudvalidation.person.application.impl;

import com.block7.crudvalidation.person.application.service.DeletePersonService;
import com.block7.crudvalidation.person.domain.repository.DeletePersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeletePersonServiceImpl implements DeletePersonService {

    private final DeletePersonRepository personRepository;

    @Override
    public void deletePersonById(Integer id) {
        personRepository.deletePersonById(id);
    }
}

