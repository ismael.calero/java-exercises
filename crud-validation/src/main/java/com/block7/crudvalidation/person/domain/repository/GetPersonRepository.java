package com.block7.crudvalidation.person.domain.repository;

import com.block7.crudvalidation.person.domain.entity.Person;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GetPersonRepository {
    Person getPersonById(Integer id) throws EntityNotFoundException;
    List<Person> getPersonByUsuario(String usuario);
    Page<Person> getAllPeople(Integer pageNumber, Integer pageSize);
}
