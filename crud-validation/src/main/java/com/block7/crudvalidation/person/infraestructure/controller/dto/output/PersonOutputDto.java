package com.block7.crudvalidation.person.infraestructure.controller.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonOutputDto {

    private Integer idPersona;

    private String usuario;

    private String password;

    private String name;

    private String surname;

    private String companyEmail;

    private String personalEmail;

    private String city;

    private Boolean active;

    private Date createdDate;

    private String imagenUrl;

    private Date terminationDate;
}
