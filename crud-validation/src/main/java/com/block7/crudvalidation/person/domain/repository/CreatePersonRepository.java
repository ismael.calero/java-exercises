package com.block7.crudvalidation.person.domain.repository;

import com.block7.crudvalidation.person.domain.entity.Person;

public interface CreatePersonRepository {
    Person createPerson(Person person);
}
