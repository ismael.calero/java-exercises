package com.block7.crudvalidation.person.application.impl;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.CreatePersonService;
import com.block7.crudvalidation.person.application.service.GetPersonService;
import com.block7.crudvalidation.person.application.service.UpdatePersonService;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.person.domain.repository.CreatePersonRepository;
import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
@AllArgsConstructor
public class UpdatePersonServiceImpl implements UpdatePersonService {

    private final CreatePersonService createPersonService;
    private final CreatePersonRepository personRepository;
    private final GetPersonService personService;
    private final PersonMapper personMapper;

    @Override
    public Person updatePerson(Integer id, PersonInputDto personInputDto) throws ResponseStatusException {

        Person person = personService.getPersonById(id);

        createPersonService.validatePersonInput(personInputDto);

        personMapper.updateToEntity(personInputDto, person);

        return personRepository.createPerson(person);
    }
}

