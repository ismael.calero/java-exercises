package com.block7.crudvalidation.person.application.service;


import com.block7.crudvalidation.person.domain.entity.Person;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GetPersonService {
    Person getPersonById(Integer id) throws EntityNotFoundException;
    List<Person> getPersonByUsuario(String usuario);
    Page<Person> getAllPeople(Integer pageNumber, Integer pageSize);
}
