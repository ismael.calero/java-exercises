package com.block7.crudvalidation.subject.infraestructure.repository.jpa;

import com.block7.crudvalidation.subject.infraestructure.repository.jpa.entity.AsignaturaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface AsignaturaRepositoryJpa extends JpaRepository<AsignaturaJpa, Integer> {
    List<AsignaturaJpa> findByAsignatura(String asignatura);
}
