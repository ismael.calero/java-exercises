package com.block7.crudvalidation.subject.domain.repository;

import com.block7.crudvalidation.subject.domain.entity.Asignatura;

public interface CreateAsignaturaRepository {
    Asignatura createAsignatura(Asignatura asignatura);
}
