package com.block7.crudvalidation.subject.domain.entity;

import lombok.*;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Asignatura {
    Integer idAsignatura;

    Integer idStudent;

    String asignatura;

    String comments;

    Date initialDate;

    Date finishDate;
}
