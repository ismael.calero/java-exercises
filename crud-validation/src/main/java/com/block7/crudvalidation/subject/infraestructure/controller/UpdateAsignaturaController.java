package com.block7.crudvalidation.subject.infraestructure.controller;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.application.service.UpdateAsignaturaService;
import com.block7.crudvalidation.subject.domain.exception.CustomError;
import com.block7.crudvalidation.subject.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.output.AsignaturaOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/asignatura")
@RequiredArgsConstructor
public class UpdateAsignaturaController {

    private final UpdateAsignaturaService asignaturaService;
    private final AsignaturaMapper asignaturaMapper;

    @PutMapping("/{id}")
    public ResponseEntity<AsignaturaOutputDto> updateAsignatura(@PathVariable Integer id, @RequestBody AsignaturaInputDto asignatura) throws Exception {
        try {
            return new ResponseEntity<>(asignaturaMapper.toOutput(asignaturaService.updateAsignatura(id, asignatura)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(asignaturaMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(asignaturaMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
