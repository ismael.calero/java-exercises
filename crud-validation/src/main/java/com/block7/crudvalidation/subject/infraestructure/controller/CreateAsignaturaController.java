package com.block7.crudvalidation.subject.infraestructure.controller;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.application.service.CreateAsignaturaService;
import com.block7.crudvalidation.subject.domain.exception.CustomError;
import com.block7.crudvalidation.subject.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.output.AsignaturaOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/asignatura")
@RequiredArgsConstructor
public class CreateAsignaturaController {

    private final CreateAsignaturaService asignaturaService;
    private final AsignaturaMapper personaMapper;

    @PostMapping
    public ResponseEntity<AsignaturaOutputDto> createAsignatura(@RequestBody AsignaturaInputDto asignatura) throws Exception {
        try {
            AsignaturaOutputDto outputDto = personaMapper.toOutput(asignaturaService.createAsignatura(asignatura));
            return new ResponseEntity<>(outputDto, HttpStatus.OK);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(personaMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}