package com.block7.crudvalidation.subject.infraestructure.repository.impl;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.repository.GetAsignaturaRepository;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.AsignaturaRepositoryJpa;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class GetAsignaturaRepositoryImpl implements GetAsignaturaRepository {

    private final AsignaturaRepositoryJpa asignaturaRepositoryJpa;
    private final AsignaturaMapper asignaturaMapper;


    @Override
    public Asignatura getAsignaturaById(Integer id) throws EntityNotFoundException {

        return asignaturaMapper.toEntityfromJpa(asignaturaRepositoryJpa.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Asignatura no encontrada")));
    }

    @Override
    public List<Asignatura> getAsignaturaByName(String asignatura) {

        return asignaturaMapper.toListEntity(asignaturaRepositoryJpa.findByAsignatura(asignatura));
    }

    @Override
    public Page<Asignatura> getAllAsignaturas(Integer pageNumber, Integer pageSize) {

        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);

        return asignaturaRepositoryJpa.findAll(pageable).map(asignaturaMapper::toEntityfromJpa);
    }
}
