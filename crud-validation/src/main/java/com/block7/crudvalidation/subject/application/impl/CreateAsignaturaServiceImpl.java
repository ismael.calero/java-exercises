package com.block7.crudvalidation.subject.application.impl;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.application.service.CreateAsignaturaService;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.repository.CreateAsignaturaRepository;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
@RequiredArgsConstructor
public class CreateAsignaturaServiceImpl implements CreateAsignaturaService {

    private final CreateAsignaturaRepository createAsignaturaRepository;
    private final AsignaturaMapper asignaturaMapper;

    @Override
    public Asignatura createAsignatura(AsignaturaInputDto asignaturaInput) throws ResponseStatusException {

        validateAsignaturaInput(asignaturaInput);

        Asignatura asignatura = asignaturaMapper.toEntity(asignaturaInput);

        return createAsignaturaRepository.createAsignatura(asignatura);
    }

    public void validateAsignaturaInput(AsignaturaInputDto asignaturaInput) throws ResponseStatusException {
//        Validacion de fecha inicial
        if (asignaturaInput.getInitialDate() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'initial date' no puede ser nulo");
        }
    }

}
