package com.block7.crudvalidation.subject.domain.repository;

public interface DeleteAsignaturaRepository {
    void deleteAsignaturaById(Integer id);
}
