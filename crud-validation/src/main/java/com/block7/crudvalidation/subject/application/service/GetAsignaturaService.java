package com.block7.crudvalidation.subject.application.service;


import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GetAsignaturaService {
    Asignatura getAsignaturaById(Integer id) throws EntityNotFoundException;
    List<Asignatura> getAsignaturaByName(String asignatura);
    Page<Asignatura> getAllAsignaturas(Integer pageNumber, Integer pageSize);
}
