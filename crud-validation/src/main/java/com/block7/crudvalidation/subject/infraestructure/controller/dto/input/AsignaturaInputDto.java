package com.block7.crudvalidation.subject.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AsignaturaInputDto {
    Integer idAsignatura;

    Integer idStudent;

    String asignatura;

    String comments;

    Date initialDate;

    Date finishDate;
}
