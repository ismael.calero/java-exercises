package com.block7.crudvalidation.subject.application.service;

import jakarta.persistence.EntityNotFoundException;

public interface DeleteAsignaturaService {
    void deleteAsignaturaById(Integer id) throws EntityNotFoundException;
}
