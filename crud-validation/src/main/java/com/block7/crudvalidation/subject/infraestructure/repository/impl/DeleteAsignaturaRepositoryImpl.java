package com.block7.crudvalidation.subject.infraestructure.repository.impl;

import com.block7.crudvalidation.subject.domain.repository.DeleteAsignaturaRepository;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.AsignaturaRepositoryJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeleteAsignaturaRepositoryImpl implements DeleteAsignaturaRepository {

    private final AsignaturaRepositoryJpa asignaturaRepositoryJpa;

    @Override
    public void deleteAsignaturaById(Integer id) {
        asignaturaRepositoryJpa.deleteById(id);
    }
}
