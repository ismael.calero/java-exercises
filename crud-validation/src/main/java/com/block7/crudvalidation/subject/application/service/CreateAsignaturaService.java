package com.block7.crudvalidation.subject.application.service;

import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import org.springframework.web.server.ResponseStatusException;

public interface CreateAsignaturaService {
    Asignatura createAsignatura(AsignaturaInputDto person) throws ResponseStatusException;
    void validateAsignaturaInput(AsignaturaInputDto personInput) throws ResponseStatusException;
}

