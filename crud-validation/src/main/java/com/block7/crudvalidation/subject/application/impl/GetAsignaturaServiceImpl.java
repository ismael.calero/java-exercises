package com.block7.crudvalidation.subject.application.impl;

import com.block7.crudvalidation.subject.application.service.GetAsignaturaService;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.repository.GetAsignaturaRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class GetAsignaturaServiceImpl implements GetAsignaturaService {

    private final GetAsignaturaRepository asignaturaRepository;

    @Override
    public Asignatura getAsignaturaById(Integer id) throws EntityNotFoundException {

        return asignaturaRepository.getAsignaturaById(id);
    }

    @Override
    public List<Asignatura> getAsignaturaByName(String name) {
        return asignaturaRepository.getAsignaturaByName(name);

    }

    @Override
    public Page<Asignatura> getAllAsignaturas(Integer pageNumber, Integer pageSize) {
        return asignaturaRepository.getAllAsignaturas(pageNumber, pageSize);
    }
}
