package com.block7.crudvalidation.subject.application.mapper;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.exception.CustomError;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.output.AsignaturaOutputDto;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.entity.AsignaturaJpa;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;


@Mapper(componentModel = "spring", uses = StudentMapper.class)
public abstract class AsignaturaMapper {

    public abstract Asignatura toEntity(AsignaturaInputDto asignatura);
    public abstract AsignaturaOutputDto toOutput(Asignatura asignatura);
    public abstract List<AsignaturaOutputDto> toListOutput(List<Asignatura> asignatura);

    public abstract AsignaturaJpa toEntityJpa(Asignatura asignatura);
    @Mapping(target = "idStudent", source = "asignatura.student.idStudent")
    public abstract Asignatura toEntityfromJpa(AsignaturaJpa asignatura);
    public abstract List<Asignatura> toListEntity(List<AsignaturaJpa> asignatura);

    public abstract AsignaturaOutputDto toOutputfromError(CustomError error);

    //@Mapping(target = "name", source = "firstName")
    //@Mapping(target = "id_persona", expression = "java( person.getSurname() != null ? person.setSurname(personInputDto.getSurname()))")
    //@AfterMapping
    public abstract void updateToEntity(AsignaturaInputDto personInputDto, @MappingTarget Asignatura asignatura);
}
