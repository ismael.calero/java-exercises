package com.block7.crudvalidation.subject.application.impl;


import com.block7.crudvalidation.subject.application.service.DeleteAsignaturaService;
import com.block7.crudvalidation.subject.application.service.GetAsignaturaService;
import com.block7.crudvalidation.subject.domain.repository.DeleteAsignaturaRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeleteAsignaturaServiceImpl implements DeleteAsignaturaService {

    private final DeleteAsignaturaRepository asignaturaRepository;
    private final GetAsignaturaService asignaturaService;

    @Override
    public void deleteAsignaturaById(Integer id) throws EntityNotFoundException {

        asignaturaService.getAsignaturaById(id);

        asignaturaRepository.deleteAsignaturaById(id);
    }
}
