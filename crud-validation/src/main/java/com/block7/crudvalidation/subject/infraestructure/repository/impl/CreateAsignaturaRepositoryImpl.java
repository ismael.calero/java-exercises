package com.block7.crudvalidation.subject.infraestructure.repository.impl;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.repository.CreateAsignaturaRepository;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.AsignaturaRepositoryJpa;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.entity.AsignaturaJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CreateAsignaturaRepositoryImpl implements CreateAsignaturaRepository {

    private final AsignaturaRepositoryJpa asignaturaRepositoryJpa;
    private final AsignaturaMapper asignaturaMapper;

    @Override
    public Asignatura createAsignatura(Asignatura asignatura) {

        AsignaturaJpa asignaturaJpa = asignaturaMapper.toEntityJpa(asignatura);

        return asignaturaMapper.toEntityfromJpa(asignaturaRepositoryJpa.save(asignaturaJpa));
    }
}
