package com.block7.crudvalidation.subject.infraestructure.repository.jpa.entity;

import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "asignatura")
public class AsignaturaJpa {
    @Id
    @Column(name = "id_asignatura")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer idAsignatura;

    @ManyToOne
    @JoinColumn(name = "id_student")
    private StudentJpa student;

    private String asignatura;

    private String comments;

    @Column(name = "initial_date", nullable = false)
    private Date initialDate;

    @Column(name = "finish_date")
    private Date finishDate;

}
