package com.block7.crudvalidation.subject.application.impl;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.application.service.CreateAsignaturaService;
import com.block7.crudvalidation.subject.application.service.GetAsignaturaService;
import com.block7.crudvalidation.subject.application.service.UpdateAsignaturaService;
import com.block7.crudvalidation.subject.domain.entity.Asignatura;
import com.block7.crudvalidation.subject.domain.repository.CreateAsignaturaRepository;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.input.AsignaturaInputDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
@AllArgsConstructor
public class UpdateAsignaturaServiceImpl implements UpdateAsignaturaService {

    private final CreateAsignaturaService createAsignaturaService;
    private final CreateAsignaturaRepository asignaturaRepository;
    private final GetAsignaturaService asignaturaService;
    private final AsignaturaMapper asignaturaMapper;

    @Override
    public Asignatura updateAsignatura(Integer id, AsignaturaInputDto asignaturaInputDto) throws ResponseStatusException {

        Asignatura asignatura = asignaturaService.getAsignaturaById(id);

        createAsignaturaService.validateAsignaturaInput(asignaturaInputDto);

        asignaturaMapper.updateToEntity(asignaturaInputDto, asignatura);

        return asignaturaRepository.createAsignatura(asignatura);
    }
}

