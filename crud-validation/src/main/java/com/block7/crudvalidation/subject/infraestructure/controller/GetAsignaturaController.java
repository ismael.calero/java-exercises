package com.block7.crudvalidation.subject.infraestructure.controller;

import com.block7.crudvalidation.subject.application.mapper.AsignaturaMapper;
import com.block7.crudvalidation.subject.application.service.GetAsignaturaService;
import com.block7.crudvalidation.subject.domain.exception.CustomError;
import com.block7.crudvalidation.subject.infraestructure.controller.dto.output.AsignaturaOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/asignatura")
@RequiredArgsConstructor
public class GetAsignaturaController {

    private final GetAsignaturaService asignaturaService;
    private final AsignaturaMapper asignaturaMapper;

    @GetMapping("/{id}")
    public ResponseEntity<AsignaturaOutputDto> getAsignaturaById(@PathVariable Integer id) throws Exception {
        try {
            return new ResponseEntity<>(asignaturaMapper.toOutput(asignaturaService.getAsignaturaById(id)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
        CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
        return new ResponseEntity<>(asignaturaMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
    }
    }

    @GetMapping("/asignatura/{asignatura}")
    public ResponseEntity<List<AsignaturaOutputDto>> getAsignaturaByName(@PathVariable String asignatura) {
        try {
            return new ResponseEntity<>(asignaturaMapper.toListOutput(asignaturaService.getAsignaturaByName(asignatura)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public ResponseEntity<Page<AsignaturaOutputDto>> getAllAsignaturas(@RequestParam(defaultValue = "0", required = false)
                                                                 Integer pageNumber,
                                                              @RequestParam(defaultValue = "5", required = false)
                                                                 Integer pageSize) {
        return new ResponseEntity<>(asignaturaService.getAllAsignaturas(pageNumber, pageSize).map(asignaturaMapper::toOutput), HttpStatus.OK);
    }
}
