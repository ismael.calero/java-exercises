package com.block7.crudvalidation.subject.infraestructure.controller.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AsignaturaOutputDto {
    Integer idAsignatura;

    Integer idStudent;

    String asignatura;

    String comments;

    Date initialDate;

    Date finishDate;
}
