package com.block7.crudvalidation.student.infraestructure.repository.impl;

import com.block7.crudvalidation.student.domain.repository.DeleteStudentRepository;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeleteStudentRepositoryImpl implements DeleteStudentRepository {

    private final StudentRepositoryJpa studentRepositoryJpa;

    @Override
    public void deleteStudentById(Integer id) {
        studentRepositoryJpa.deleteById(id);
    }
}
