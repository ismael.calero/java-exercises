package com.block7.crudvalidation.student.application.service;


import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import org.springframework.web.server.ResponseStatusException;

public interface UpdateStudentService {
    Student updateStudent(Integer id, StudentInputDto student) throws ResponseStatusException;
}
