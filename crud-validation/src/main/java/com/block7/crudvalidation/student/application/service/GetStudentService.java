package com.block7.crudvalidation.student.application.service;


import com.block7.crudvalidation.student.domain.entity.Student;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;


public interface GetStudentService {
    Student getStudentById(Integer id) throws EntityNotFoundException;
    Page<Student> getAllStudents(Integer pageNumber, Integer pageSize);
}
