package com.block7.crudvalidation.student.application.impl;

import com.block7.crudvalidation.student.application.service.DeleteStudentService;
import com.block7.crudvalidation.student.application.service.GetStudentService;
import com.block7.crudvalidation.student.domain.repository.DeleteStudentRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class DeleteStudentServiceImpl implements DeleteStudentService {

    private final DeleteStudentRepository studentRepository;
    private final GetStudentService studentService;

    @Override
    public void deleteStudentById(Integer id) throws EntityNotFoundException {

        studentService.getStudentById(id);

        studentRepository.deleteStudentById(id);
    }
}
