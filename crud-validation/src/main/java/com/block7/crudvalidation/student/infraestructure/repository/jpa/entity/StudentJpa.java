package com.block7.crudvalidation.student.infraestructure.repository.jpa.entity;

import com.block7.crudvalidation.person.infraestructure.repository.jpa.entity.PersonJpa;
import com.block7.crudvalidation.professor.infraestructure.repository.jpa.entity.ProfesorJpa;
import com.block7.crudvalidation.subject.infraestructure.repository.jpa.entity.AsignaturaJpa;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "student")
public class StudentJpa {

    @Id
    @Column(name = "id_student")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer idStudent;

    @OneToOne
    @JoinColumn(name = "id_persona")
    private PersonJpa persona;

    @Column(name = "num_hours_week")
    private Integer numHoursWeek;

    private String comments;

    @ManyToOne
    @JoinColumn(name = "id_profesor")
    private ProfesorJpa profesor;

    @Column(nullable = false)
    private String branch;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    private List<AsignaturaJpa> asignaturas;
}

