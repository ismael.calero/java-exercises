package com.block7.crudvalidation.student.application.impl;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.CreateStudentService;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.repository.CreateStudentRepository;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class CreateStudentServiceImpl implements CreateStudentService {

    private final CreateStudentRepository createStudentRepository;
    private final StudentMapper studentMapper;

    @Override
    public Student createStudent(StudentInputDto studentInput) throws ResponseStatusException {

        validateStudentInput(studentInput);

        Student student = studentMapper.toEntity(studentInput);

        return createStudentRepository.createStudent(student);
    }

    public void validateStudentInput(StudentInputDto studentInput) throws ResponseStatusException {
        if (studentInput.getNumHoursWeek() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'numero de horas a la semana' no puede ser nulo");
        }
        if (studentInput.getBranch() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"El campo 'branch' no puede ser nulo");
        }
    }

}
