package com.block7.crudvalidation.student.infraestructure.controller;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.UpdateStudentService;
import com.block7.crudvalidation.student.domain.exception.CustomError;
import com.block7.crudvalidation.student.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class UpdateStudentController {

    private final UpdateStudentService studentService;
    private final StudentMapper studentMapper;

    @PutMapping("/{id}")
    public ResponseEntity<StudentOutputDto> updateStudent(@PathVariable Integer id, @RequestBody StudentInputDto student) throws Exception {
        try {
            return new ResponseEntity<>(studentMapper.toOutput(studentService.updateStudent(id, student)), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(studentMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(studentMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
