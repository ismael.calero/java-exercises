package com.block7.crudvalidation.student.application.service;

import jakarta.persistence.EntityNotFoundException;

public interface DeleteStudentService {
    void deleteStudentById(Integer id) throws EntityNotFoundException;
}
