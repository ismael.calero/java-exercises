package com.block7.crudvalidation.student.infraestructure.controller;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.application.service.GetPersonService;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.GetStudentService;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.exception.CustomError;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentFullOutputDto;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentOutputDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class GetStudentController {

    private final GetStudentService studentService;
    private final GetPersonService personService;
    private final StudentMapper studentMapper;
    private final PersonMapper personMapper;

    @GetMapping("/{id}")
    public ResponseEntity<StudentOutputDto> getStudentById(@PathVariable Integer id, @RequestParam(defaultValue = "simple") String outputType) throws Exception {
        try {
            Student student = studentService.getStudentById(id);
            Person person = personService.getPersonById(id);

            if (outputType.equals("simple")) {
                StudentOutputDto simpleOutputDto = studentMapper.toOutput(student);
                return new ResponseEntity<>(simpleOutputDto, HttpStatus.OK);
            } else if (outputType.equals("full")) {
                StudentFullOutputDto fullOutputDto = studentMapper.toFullOutput(student, person);
                return new ResponseEntity<>(studentMapper.toOutputfromFull(fullOutputDto), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(studentMapper.toOutputfromError(error), HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping
    public ResponseEntity<Page<StudentOutputDto>> getAllStudents(@RequestParam(defaultValue = "0", required = false)
                                                                 Integer pageNumber,
                                                              @RequestParam(defaultValue = "5", required = false)
                                                                 Integer pageSize) {
        return new ResponseEntity<>(studentService.getAllStudents(pageNumber, pageSize).map(studentMapper::toOutput), HttpStatus.OK);
    }
}
