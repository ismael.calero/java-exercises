package com.block7.crudvalidation.student.infraestructure.repository.impl;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.repository.CreateStudentRepository;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CreateStudentRepositoryImpl implements CreateStudentRepository {

    private final StudentRepositoryJpa studentRepositoryJpa;
    private final StudentMapper studentMapper;

    @Override
    public Student createStudent(Student student) {

        StudentJpa studentJpa = studentMapper.toEntityJpa(student);

        return studentMapper.toEntityfromJpa(studentRepositoryJpa.save(studentJpa));
    }
}
