package com.block7.crudvalidation.student.infraestructure.repository.impl;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.repository.GetStudentRepository;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class GetStudentRepositoryImpl implements GetStudentRepository {

    private final StudentRepositoryJpa studentRepositoryJpa;
    private final StudentMapper studentMapper;


    @Override
    public Student getStudentById(Integer id) throws EntityNotFoundException {

        return studentMapper.toEntityfromJpa(studentRepositoryJpa.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Persona no encontrada")));
    }

    @Override
    public Page<Student> getAllStudents(Integer pageNumber, Integer pageSize) {

        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);

        return studentRepositoryJpa.findAll(pageable).map(studentMapper::toEntityfromJpa);
    }
}
