package com.block7.crudvalidation.student.infraestructure.controller;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.CreateStudentService;
import com.block7.crudvalidation.student.domain.exception.CustomError;
import com.block7.crudvalidation.student.domain.exception.UnprocessableEntityException;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentOutputDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class CreateStudentController {

    private final CreateStudentService studentService;
    private final StudentMapper studentMapper;

    @PostMapping
    public ResponseEntity<StudentOutputDto> createStudent(@RequestBody StudentInputDto student) throws Exception {
        try {
            StudentOutputDto outputDto = studentMapper.toOutput(studentService.createStudent(student));
            return new ResponseEntity<>(outputDto, HttpStatus.OK);
        } catch (UnprocessableEntityException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage());
            return new ResponseEntity<>(studentMapper.toOutputfromError(error), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}