package com.block7.crudvalidation.student.infraestructure.repository.jpa;

import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StudentRepositoryJpa extends JpaRepository<StudentJpa, Integer> {}
