package com.block7.crudvalidation.student.application.impl;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.CreateStudentService;
import com.block7.crudvalidation.student.application.service.GetStudentService;
import com.block7.crudvalidation.student.application.service.UpdateStudentService;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.repository.CreateStudentRepository;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
@AllArgsConstructor
public class UpdateStudentServiceImpl implements UpdateStudentService {

    private final CreateStudentService createStudentService;
    private final CreateStudentRepository studentRepository;
    private final GetStudentService studentService;
    private final StudentMapper studentMapper;

    @Override
    public Student updateStudent(Integer id, StudentInputDto studentInputDto) throws ResponseStatusException {

        Student student = studentService.getStudentById(id);

        createStudentService.validateStudentInput(studentInputDto);

        studentMapper.updateToEntity(studentInputDto, student);

        return studentRepository.createStudent(student);
    }
}

