package com.block7.crudvalidation.student.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentInputDto {
    Integer idStudent;

    Integer idPersona;

    Integer numHoursWeek;

    String comments;

    Integer idProfesor;

    String branch;
}
