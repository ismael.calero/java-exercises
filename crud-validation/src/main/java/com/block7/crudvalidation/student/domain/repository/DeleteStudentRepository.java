package com.block7.crudvalidation.student.domain.repository;

public interface DeleteStudentRepository {
    void deleteStudentById(Integer id);
}
