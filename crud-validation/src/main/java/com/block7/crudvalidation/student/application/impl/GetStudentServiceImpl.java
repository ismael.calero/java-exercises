package com.block7.crudvalidation.student.application.impl;

import com.block7.crudvalidation.student.application.service.GetStudentService;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.repository.GetStudentRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class GetStudentServiceImpl implements GetStudentService {

    private final GetStudentRepository studentRepository;

    @Override
    public Student getStudentById(Integer id) throws EntityNotFoundException {

        return studentRepository.getStudentById(id);
    }

    @Override
    public Page<Student> getAllStudents(Integer pageNumber, Integer pageSize) {
        return studentRepository.getAllStudents(pageNumber, pageSize);
    }
}
