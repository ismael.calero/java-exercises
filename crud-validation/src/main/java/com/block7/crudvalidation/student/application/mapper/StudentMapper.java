package com.block7.crudvalidation.student.application.mapper;

import com.block7.crudvalidation.person.application.mapper.PersonMapper;
import com.block7.crudvalidation.person.domain.entity.Person;
import com.block7.crudvalidation.professor.application.mapper.ProfesorMapper;
import com.block7.crudvalidation.student.domain.entity.Student;
import com.block7.crudvalidation.student.domain.exception.CustomError;
import com.block7.crudvalidation.student.infraestructure.controller.dto.input.StudentInputDto;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentFullOutputDto;
import com.block7.crudvalidation.student.infraestructure.controller.dto.output.StudentOutputDto;
import com.block7.crudvalidation.student.infraestructure.repository.jpa.entity.StudentJpa;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

// Para poder mappear un objeto de un modulo distinto como person o profesor, necesitamos importar los mapper de dichos objetos con uses
@Mapper(componentModel = "spring", uses = {PersonMapper.class, ProfesorMapper.class})
public abstract class StudentMapper {

    public abstract Student toEntity(StudentInputDto student);
    public abstract StudentOutputDto toOutput(Student student);
    public abstract List<StudentOutputDto> toListOutput(List<Student> student);

    public abstract StudentJpa toEntityJpa(Student student);
    @Mapping(target = "idPersona", source = "student.persona.idPersona")
    @Mapping(target = "idProfesor", source = "student.profesor.idProfesor")
    public abstract Student toEntityfromJpa(StudentJpa student);
    public abstract List<Student> toListEntity(List<StudentJpa> student);

    public abstract StudentOutputDto toOutputfromError(CustomError error);

    //@Mapping(target = "name", source = "firstName")
    //@Mapping(target = "id_persona", expression = "java( person.getSurname() != null ? person.setSurname(personInputDto.getSurname()))")
    //@AfterMapping
    public abstract void updateToEntity(StudentInputDto studentInputDto, @MappingTarget Student student);

    @Mapping(target = "idPersona", source = "person.idPersona")
    public abstract StudentFullOutputDto toFullOutput(Student student, Person person);
    public abstract StudentOutputDto toOutputfromFull(StudentFullOutputDto student);
}
