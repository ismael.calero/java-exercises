package com.block7.crudvalidation.student.domain.repository;

import com.block7.crudvalidation.student.domain.entity.Student;

public interface CreateStudentRepository {
    Student createStudent(Student student);
}
