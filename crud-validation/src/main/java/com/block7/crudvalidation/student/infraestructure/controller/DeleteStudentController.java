package com.block7.crudvalidation.student.infraestructure.controller;

import com.block7.crudvalidation.student.application.mapper.StudentMapper;
import com.block7.crudvalidation.student.application.service.DeleteStudentService;
import com.block7.crudvalidation.student.domain.exception.CustomError;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class DeleteStudentController {

    private final DeleteStudentService studentService;

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudentById(@PathVariable int id) throws Exception {
        try {
            studentService.deleteStudentById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (EntityNotFoundException e) {
            CustomError error = new CustomError(new Date(), HttpStatus.NOT_FOUND.value(), e.getMessage());
            return new ResponseEntity<>(error.toString(), HttpStatus.NOT_FOUND);
        }
    }
}
