package com.block7.crudvalidation.student.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    Integer idStudent;

    Integer idPersona;

    Integer numHoursWeek;

    String comments;

    Integer idProfesor;

    String branch;
}
