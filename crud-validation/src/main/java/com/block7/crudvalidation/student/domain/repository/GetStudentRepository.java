package com.block7.crudvalidation.student.domain.repository;

import com.block7.crudvalidation.student.domain.entity.Student;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;


public interface GetStudentRepository {
    Student getStudentById(Integer id) throws EntityNotFoundException;
    Page<Student> getAllStudents(Integer pageNumber, Integer pageSize);
}
