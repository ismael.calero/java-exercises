package com.block7.crudvalidation.service;

//import com.block7.crudvalidation.person.application.impl.UpdatePersonServiceImpl;
//import com.block7.crudvalidation.person.application.mapper.PersonMapper;
//import com.block7.crudvalidation.person.application.service.CreatePersonService;
//import com.block7.crudvalidation.person.application.service.GetPersonService;
//import com.block7.crudvalidation.person.domain.entity.Person;
//import com.block7.crudvalidation.person.domain.repository.CreatePersonRepository;
//import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
//import org.junit.Test;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Calendar;
//import java.util.Date;
//
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@SpringBootTest
//@DisplayName("Tests for Person Service")
//@ExtendWith(MockitoExtension.class)
//public class UpdatePersonServiceTest {
//
//    @Mock
//    private CreatePersonService createPersonService;
//
//    @Mock
//    private CreatePersonRepository personRepository;
//
//    @Mock
//    private GetPersonService personService;
//
//    @Mock
//    private PersonMapper personMapper;
//
//    @InjectMocks
//    private UpdatePersonServiceImpl updatePersonService;
//
//    @Test
//    @DisplayName("Test for updating a person")
//    public void updatePerson() {
//        // Arrange
//        Integer id = 1;
//        PersonInputDto personInputDto = PersonInputDto.builder()
//                .idPersona(id)
//                .usuario("validUser")
//                .password("validPassword")
//                .name("Updated Name")
//                .companyEmail("updated@example.com")
//                .personalEmail("updated.personal@example.com")
//                .city("Updated City")
//                .active(true)
//                .createdDate(new Date(2024, Calendar.APRIL, 25))
//                .build();
//
//        Person existingPerson = Person.builder()
//                .idPersona(id)
//                .usuario("existingUser")
//                .password("existingPassword")
//                .name("Existing Name")
//                .companyEmail("existing@example.com")
//                .personalEmail("existing.personal@example.com")
//                .city("Existing City")
//                .active(true)
//                .createdDate(new Date(2024, Calendar.APRIL, 25))
//                .build();
//
//        Person updatedPerson = Person.builder()
//                .idPersona(id)
//                .usuario("validUser")
//                .password("validPassword")
//                .name("Updated Name")
//                .companyEmail("updated@example.com")
//                .personalEmail("updated.personal@example.com")
//                .city("Updated City")
//                .active(true)
//                .createdDate(new Date(2024, Calendar.APRIL, 25))
//                .build();
//
//        when(personService.getPersonById(existingPerson.getIdPersona())).thenReturn(existingPerson);
//        when(personRepository.createPerson(updatedPerson)).thenReturn(updatedPerson);
//
//        // Act
//        Person result = updatePersonService.updatePerson(id, personInputDto);
//
//        // Assert
//        verify(personService).getPersonById(existingPerson.getIdPersona());
//        verify(personRepository).createPerson(existingPerson);
//        assertNotEquals(existingPerson, updatedPerson);
//        Assertions.assertEquals(updatedPerson, result);
//    }
//}
