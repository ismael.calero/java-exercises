package com.block7.crudvalidation.service;

//import com.block7.crudvalidation.person.application.impl.GetPersonServiceImpl;
//import com.block7.crudvalidation.person.domain.entity.Person;
//import com.block7.crudvalidation.person.domain.repository.GetPersonRepository;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.mockito.Mockito.when;
//
//
//@SpringBootTest
//@DisplayName("Tests for Person Service")
//@ExtendWith(MockitoExtension.class)
//class GetPersonServiceTest {
//
//    @Autowired
//    private GetPersonRepository personRepository;
//
//    @InjectMocks
//    private GetPersonServiceImpl getPersonService;
//
//    @BeforeEach
//    void setUp() {
//    }
//
//    @Test
//    @DisplayName("Test for getting a person by ID")
//    public void getPersonById() {
//        // Arrange
//        Integer id = 1;
//        Person expectedPerson = Person.builder()
//                .idPersona(1)
//                .usuario("validUser")
//                .password("validPassword")
//                .name("John")
//                .companyEmail("john@example.com")
//                .personalEmail("john.personal@example.com")
//                .city("New York")
//                .active(true)
//                .createdDate(new Date())
//                .build();
//
//        when(personRepository.getPersonById(id)).thenReturn(expectedPerson);
//
//        // Act
//        Person actualPerson = getPersonService.getPersonById(id);
//
//        // Assert
//        assertNotNull(actualPerson);
//        assertEquals(expectedPerson, actualPerson);
//    }
//
//    @Test
//    void getPersonByUsuario() {
//        // Arrange
//        String usuario = "validUser";
//        List<Person> expectedPeople = Arrays.asList(
//                Person.builder()
//                        .idPersona(1)
//                        .usuario(usuario)
//                        .password("validPassword")
//                        .name("John")
//                        .companyEmail("john@example.com")
//                        .personalEmail("john.personal@example.com")
//                        .city("New York")
//                        .active(true)
//                        .createdDate(new Date(2024, Calendar.APRIL, 25))
//                        .build(),
//                Person.builder()
//                        .idPersona(2)
//                        .usuario(usuario)
//                        .password("anotherPassword")
//                        .name("Jane")
//                        .companyEmail("jane@example.com")
//                        .personalEmail("jane.personal@example.com")
//                        .city("Los Angeles")
//                        .active(true)
//                        .createdDate(new Date(2024, Calendar.APRIL, 26))
//                        .build()
//        );
//
//        when(personRepository.getPersonByUsuario(usuario)).thenReturn(expectedPeople);
//
//        // Act
//        List<Person> result = getPersonService.getPersonByUsuario(usuario);
//
//        // Assert
//        Assertions.assertEquals(expectedPeople, result);
//    }
//
//    @Test
//    void getAllPeople() {
//        // Arrange
//        int pageNumber = 1;
//        int pageSize = 10;
//
//        List<Person> expectedPeople = List.of(
//                Person.builder()
//                        .idPersona(1)
//                        .usuario("user1")
//                        .password("password1")
//                        .name("John Doe")
//                        .companyEmail("john@example.com")
//                        .personalEmail("john.personal@example.com")
//                        .city("New York")
//                        .active(true)
//                        .createdDate(new Date(2024, Calendar.APRIL, 25))
//                        .build(),
//                Person.builder()
//                        .idPersona(2)
//                        .usuario("user2")
//                        .password("password2")
//                        .name("Jane Doe")
//                        .companyEmail("jane@example.com")
//                        .personalEmail("jane.personal@example.com")
//                        .city("Los Angeles")
//                        .active(true)
//                        .createdDate(new Date(2024, Calendar.APRIL, 26))
//                        .build()
//        );
//
//        Page<Person> expectedPage = new PageImpl<>(expectedPeople);
//
//        when(personRepository.getAllPeople(pageNumber, pageSize)).thenReturn(expectedPage);
//
//        // Act
//        Page<Person> result = getPersonService.getAllPeople(pageNumber, pageSize);
//
//        // Assert
//        Assertions.assertEquals(expectedPage, result);
//    }
//}