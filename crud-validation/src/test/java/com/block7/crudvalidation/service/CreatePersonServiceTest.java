package com.block7.crudvalidation.service;

//import com.block7.crudvalidation.person.application.impl.CreatePersonServiceImpl;
//import com.block7.crudvalidation.person.application.mapper.PersonMapper;
//import com.block7.crudvalidation.person.domain.entity.Person;
//import com.block7.crudvalidation.person.domain.repository.CreatePersonRepository;
//
//import com.block7.crudvalidation.person.infraestructure.controller.dto.input.PersonInputDto;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Objects;
//
//import static org.mockito.Mockito.when;
//
//@SpringBootTest
//@DisplayName("Tests for Person Service")
//@ExtendWith(MockitoExtension.class)
//public class CreatePersonServiceTest {
//
//    @Mock
//    private PersonMapper personMapper;
//
//    @Mock
//    private CreatePersonRepository createPersonRepository;
//
//    @InjectMocks
//    private CreatePersonServiceImpl createPersonService;
//
//
//    @Test
//    void createPerson() {
//
//        // Arrange
//        Person persona = Person.builder()
//                .idPersona(1)
//                .usuario("validUse")
//                .password("validPassword")
//                .name("John")
//                .companyEmail("john@example.com")
//                .personalEmail("john.personal@example.com")
//                .city("New York")
//                .active(true)
//                .createdDate(new Date(2024, Calendar.APRIL, 25))
//                .build();
//
//        PersonInputDto personaInput = PersonInputDto.builder()
//                .idPersona(1)
//                .usuario("validUse")
//                .password("validPassword")
//                .name("John")
//                .companyEmail("john@example.com")
//                .personalEmail("john.personal@example.com")
//                .city("New York")
//                .active(true)
//                .createdDate(new Date(2024, Calendar.APRIL, 25))
//                .build();
//
//        // Act
//        when(createPersonRepository.createPerson(persona)).thenReturn(persona);
//        when(personMapper.toEntity(personaInput)).thenReturn(persona);
//        Assertions.assertTrue(Objects.nonNull(createPersonService.createPerson(personaInput)));
//    }
//
//}



