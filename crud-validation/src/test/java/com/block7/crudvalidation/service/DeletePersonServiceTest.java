package com.block7.crudvalidation.service;

//import com.block7.crudvalidation.person.application.impl.DeletePersonServiceImpl;
//import com.block7.crudvalidation.person.domain.entity.Person;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Date;
//
//import static org.mockito.Mockito.*;
//
//
//@SpringBootTest
//@DisplayName("Tests for Person Service")
//@ExtendWith(MockitoExtension.class)
//class DeletePersonServiceTest {
//
//    @Mock
//    private DeletePersonServiceImpl deletePersonService;
//
//    @BeforeEach
//    void setUp() {
//    }
//
//    @Test
//    @DisplayName("Test for deleting a person by ID")
//    public void deletePersonById() {
//        // Arrange
//        Person personToDelete = Person.builder()
//                .idPersona(1)
//                .usuario("john")
//                .password("password")
//                .name("Johnsy")
//                .surname("Doe")
//                .companyEmail("john@example.com")
//                .personalEmail("john.personal@example.com")
//                .city("New York")
//                .active(true)
//                .createdDate(new Date())
//                .imagenUrl("http://example.com/image.jpg")
//                .terminationDate(null)
//                .build();
//
//        // Act
//        deletePersonService.deletePersonById(personToDelete.getIdPersona());
//
//        // Assert
//        verify(deletePersonService, times(1)).deletePersonById(personToDelete.getIdPersona());
//        verify(deletePersonService, never()).deletePersonById(null);
//
//    }
//}