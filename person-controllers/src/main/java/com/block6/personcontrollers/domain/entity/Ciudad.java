package com.block6.personcontrollers.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Getter
@Setter
@AllArgsConstructor
public class Ciudad {
    private String nombre;
    private int numeroHabitantes;
}
