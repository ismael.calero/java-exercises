package com.block6.personcontrollers.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Getter
@Setter
@AllArgsConstructor
public class Persona {
    private String nombre;
    private String poblacion;
    private int edad;

    public Persona() {

    }
}


