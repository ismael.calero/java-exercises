package com.block6.personcontrollers.application.services;

import com.block6.personcontrollers.domain.entity.Persona;
import lombok.Getter;
import org.springframework.stereotype.Service;


@Getter
@Service
public class PersonaService {

    private Persona persona = new Persona();

    public Persona createPersona(String nombre, String poblacion, int edad) {
        persona.setNombre(nombre);
        persona.setPoblacion(poblacion);
        persona.setEdad(edad);
        return persona;
    }

}


