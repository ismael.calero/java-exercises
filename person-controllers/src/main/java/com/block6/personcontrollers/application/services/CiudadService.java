package com.block6.personcontrollers.application.services;

import com.block6.personcontrollers.domain.entity.Ciudad;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Getter
@Service
public class CiudadService {
    private List<Ciudad> ciudades = new ArrayList<>();

    public void addCiudad(Ciudad ciudad) {
        ciudades.add(ciudad);
    }

}

