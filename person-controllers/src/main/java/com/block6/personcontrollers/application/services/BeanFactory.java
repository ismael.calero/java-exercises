package com.block6.personcontrollers.application.services;

import com.block6.personcontrollers.domain.entity.Persona;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanFactory {

    @Bean
    @Qualifier("bean1")
    public Persona bean1() {
        Persona persona = new Persona();
        persona.setNombre("bean1");
        persona.setPoblacion("Madrid");
        persona.setEdad(34);
        return persona;
    }

    @Bean
    @Qualifier("bean2")
    public Persona bean2() {
        Persona persona = new Persona();
        persona.setNombre("bean2");
        persona.setPoblacion("New York");
        persona.setEdad(29);
        return persona;
    }

    @Bean
    @Qualifier("bean3")
    public Persona bean3() {
        Persona persona = new Persona();
        persona.setNombre("bean3");
        persona.setPoblacion("Jaen");
        persona.setEdad(25);
        return persona;
    }
}

