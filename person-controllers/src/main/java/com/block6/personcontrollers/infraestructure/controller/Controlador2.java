package com.block6.personcontrollers.infraestructure.controller;


import com.block6.personcontrollers.application.services.CiudadService;
import com.block6.personcontrollers.application.services.PersonaService;
import com.block6.personcontrollers.domain.entity.Ciudad;
import com.block6.personcontrollers.domain.entity.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/controlador2")
public class Controlador2 {

    @Autowired
    private PersonaService personaService;
    @Autowired
    private CiudadService ciudadService;

    @Autowired
    @Qualifier("bean1")
    private Persona bean1;

    @Autowired
    @Qualifier("bean2")
    private Persona bean2;

    @Autowired
    @Qualifier("bean3")
    private Persona bean3;

    @GetMapping("/getPersona")
    public Persona getPersona() {
        Persona persona = personaService.getPersona();
        persona.setEdad(persona.getEdad() * 2);
        return persona;
    }

    @GetMapping("/getCiudades")
    public List<Ciudad> getCiudades() {
        return ciudadService.getCiudades();
    }

    @GetMapping("/bean/{bean}")
    public Persona getBean(@PathVariable String bean) {
        return switch (bean) {
            case "bean1" -> bean1;
            case "bean2" -> bean2;
            case "bean3" -> bean3;
            default -> null;
        };
    }
}
