package com.block6.personcontrollers.infraestructure.controller;


import com.block6.personcontrollers.application.services.CiudadService;
import com.block6.personcontrollers.application.services.PersonaService;
import com.block6.personcontrollers.domain.entity.Ciudad;
import com.block6.personcontrollers.domain.entity.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/controlador1")
public class Controlador1 {

    @Autowired
    private PersonaService personaService;
    @Autowired
    private CiudadService ciudadService;


    @GetMapping("/addPersona")
    public Persona addPersona(@RequestHeader String nombre, @RequestHeader String poblacion, @RequestHeader int edad) {
        return personaService.createPersona(nombre, poblacion, edad);
    }

    @PostMapping("/addCiudad")
    public void addCiudad(@RequestBody Ciudad ciudad) {
        ciudadService.addCiudad(ciudad);
    }
}

