package com.block5.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;


@SpringBootApplication
public class Main implements CommandLineRunner {

    @Value("${greeting}")
    private String greeting;

    @Value("${my.number}")
    private int myNumber;

    @Value("${new.property:new.property no tiene valor}")
    private String newProperty;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("El valor de greeting es: " + greeting);
        System.out.println("El valor de my.number es: " + myNumber);
        System.out.println("El valor de new.property es: " + newProperty);
    }
}

/* Tambien se podría usar @ConfigurationProperties funciona de forma optima cuando las propiedades tienen el mismo
prefijo facilitando la configuración y asignación automática de propiedades a los atributos. Como en este caso cada
propiedad tiene un prefijo diferente es mejor opción usar @Value*/

// Con el archivo application.yml convirtiendo los datos a dicho formato funciona igual
