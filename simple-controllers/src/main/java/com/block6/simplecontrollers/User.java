package com.block6.simplecontrollers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class User {
    private String nombre;
    private String poblacion;
    private int edad;

}