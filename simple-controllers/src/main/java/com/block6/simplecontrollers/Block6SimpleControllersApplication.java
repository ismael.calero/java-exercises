package com.block6.simplecontrollers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@SpringBootApplication
public class Block6SimpleControllersApplication {

	public static void main(String[] args) {
		SpringApplication.run(Block6SimpleControllersApplication.class, args);
	}

}

