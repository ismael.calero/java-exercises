package com.block6.simplecontrollers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class SimpleController {

    @GetMapping("/user/{nombre}")
    public String nameUser(@PathVariable String nombre) {
        return "Hola " + nombre;
    }

    @PostMapping("/useradd")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> addUser(@RequestBody User user) {
        user.setEdad(user.getEdad() + 1);
        System.out.println("Tu nombre es "+user.getNombre()+" tu edad es "+user.getEdad()+" y vives en "+user.getPoblacion());
        return new ResponseEntity<User>(user, HttpStatus.CREATED);
    }
}
