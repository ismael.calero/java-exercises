package com.block5.profiles;

import lombok.Getter;

@Getter
public class WriteImpl implements IWrite{

    private String profile;

	public WriteImpl(String profile) {
        this.profile=profile;
    }

    @Override
    public void writeLog(String log) {
        System.out.println("Profile: " + profile + " -> " + log);

    }
}
