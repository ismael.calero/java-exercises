package com.block5.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@SpringBootApplication
@Configuration
public class AppConfig {

    @Autowired
    private Environment environment;

    public AppConfig(Environment environment) {
        this.environment = environment;
    }

    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }

    @Bean
    @Profile("local")
    IWrite getWriterLocal() {
        return new WriteImpl("..local.. " + getProfile());
    }

    @Bean
    @Profile("int")
    IWrite getWriterInt() {
        return new WriteImpl("..int.. " + getProfile());
    }

    @Bean
    @Profile("pro")
    IWrite getWriterPro() {
        return new WriteImpl("..pro.. " + getProfile());
    }

    String getProfile() {
        environment.getActiveProfiles();
        String[] profiles=environment.getActiveProfiles();
        return profiles.length>0?profiles[0]:"default";
    }
}
