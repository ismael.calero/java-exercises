package com.block5.profiles;

public interface IWrite {
    public void writeLog(String log);
    public String getProfile();
}