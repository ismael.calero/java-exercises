package com.block5.profiles;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


@Slf4j
@SpringBootApplication
public class Block5ProfilesApplication implements CommandLineRunner {

	private final String environment;

	public Block5ProfilesApplication(@Value("${environment}") String environment) {
		this.environment = environment;
	}

	public static void main(String[] args) {
		SpringApplication.run(Block5ProfilesApplication.class, args);
	}

	@Override
	public void run(String... args) {
		Properties properties = loadProperties(environment);

		System.out.println("Entorno: " + environment);
		System.out.println("Perfil activo: " + properties.getProperty("spring.profiles.active"));
		System.out.println("URL de la base de datos: " + properties.getProperty("bd.url"));
	}

	private Properties loadProperties(String environment) {
		String filename = environment + ".properties";
		Properties properties = new Properties();

		try (InputStream inputStream = Block5ProfilesApplication.class.getClassLoader().getResourceAsStream(filename)) {
			if (inputStream == null) {
				throw new FileNotFoundException("Archivo de propiedades no encontrado: " + filename);
			}
			properties.load(inputStream);
		} catch (IOException e) {
			throw new RuntimeException("Error al cargar las propiedades del entorno " + environment, e);
		}

		return properties;
	}
}

